/**
 * Fixes the text content by applying multiple replacements and updates the output elements.
 * @function
 * @param {string} origin - The origin of the text content ('edited' or other).
 * @returns {void}
 * Replaces multiple patterns in the text to clean it up:
 * - Replaces line breaks with a space.
 * - Reduces multiple spaces to a single space.
 * - Removes leading spaces at the beginning of lines.
 * - Removes trailing spaces at the end of lines.
 */
function fixText(origin) {
    let text = '';
    if (origin == 'edited') {
        text = document.getElementById('editText').value;
    } else {
        text = document.getElementById('inputText').value;
    }

    let fixedText = text
        .replace(/(\r\n|\n|\r)(?!\n)/gm, ' ')
        .replace(/\s{2,}/g, ' ')
        .replace(/(^|\n|\r)\s+/g, '')
        .replace(/\s+($|\n|\r)/g, '');

    document.getElementById('outputText').value = fixedText;
    document.getElementById('editText').value = fixedText;

    copyToClipboard(fixedText);
}

/**
 * Clears the text content of the input element.
 * @function
 * @returns {void}
 */
function clearTextArea() {
    document.getElementById('inputText').value = '';
}

/**
 * Copies the provided text to the clipboard and shows a notification.
 * @function
 * @param {string} text - The text to copy to the clipboard.
 * @returns {void}
 * Writes the text to the clipboard and displays a notification based on success or failure.
 */
function copyToClipboard(text) {
    navigator.clipboard.writeText(text).then(
        function () {
            showNotification('Text copied to clipboard!', 'green');
        },
        function (err) {
            showNotification('Failed to copy text.', 'red');
        }
    );
}

/**
 * Shows a notification message with the specified content and color.
 * @function
 * @param {string} message - The message to display in the notification.
 * @param {string} color - The color of the notification (green or red).
 * @returns {void}
 * Displays a notification with the given message and color, then hides it after 3 seconds.
 */
function showNotification(message, color) {
    const notification = document.getElementById('notification');
    notification.style.display = 'block';
    notification.textContent = message;
    notification.style.backgroundColor =
        color === 'green' ? '#28a745' : '#dc3545';

    /**
     * Hides the notification after 3 seconds.
     * @callback
     * @returns {void}
     */
    setTimeout(function () {
        notification.style.display = 'none';
    }, 3000);
}

/**
 * Adds an event listener to handle paste events on the input element when the DOM content is loaded.
 * @callback
 */
document.addEventListener('DOMContentLoaded', function () {
    let inputText = document.getElementById('inputText');

    inputText.addEventListener('paste', function (e) {
        setTimeout(function () {
            fixText('pasted');
        }, 100);
    });
});
