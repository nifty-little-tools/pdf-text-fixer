# PDF Text Fixer - PTF

## Story

The PTF is a tool born out of necessity. It has always annoyed me that when you copy some text from a PDF, it copies each line as a line break, even though it visually looks like a continuos sentence in a paragraph when you see the PDF, and I wanted to preserve that when copying it.

## Features

The PTF is a web-based tool that allows you paste copied PDF text and remove the annoying breaks and extra whitespaces. Here are some of its key features:

1. **Text Correction Support:** The editor supports text correction, allowing you to fix any errors or make changes to the copied text from your PDF files.

2. **Semi-Live Preview:** When the text is "Fixed", you will see the results in as an output in the "Edit" area, ready to be fixed again if necessary and already copied to your clipboard for easy pasting elsewhere.

## Usage

To use the PTF, simply open the `index.html` file in your web browser if you have cloned or downloaded the repository, or navigate to <https://ptf.niftylittletools.dev/> to use the tool online.
